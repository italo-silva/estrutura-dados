package br.com.italosilva;

import br.com.italosilva.domain.Clube;
import br.com.italosilva.estutura.Vetor;

public class MainVetor {

    public static void main(String[] args) {

        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);

        Clube botafogo = new Clube("Botafogo", 0);

        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Vetor vetor = new Vetor();

        vetor.adiciona(corinthians);
        vetor.adiciona(audaxOsasco);

        Clube retornoPega = vetor.pega(1);
        System.out.println("--------> Retorno pega :"+retornoPega);

        boolean contemClube = vetor.contem(botafogo);
        System.out.println("--------> Contem clube :"+contemClube);

        int quantidadeClube = vetor.tamanho();
        System.out.println("--------> Quantidade clubes :"+quantidadeClube);

        vetor.adiciona(2, oesteBarueri);

        vetor.remove(2);


        System.out.println(vetor.tamanho());

    }
}
