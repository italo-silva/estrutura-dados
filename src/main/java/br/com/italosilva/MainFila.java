package br.com.italosilva;

import br.com.italosilva.domain.Clube;
import br.com.italosilva.estutura.Fila;
import br.com.italosilva.estutura.Pilha;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class MainFila {

    public static void main(String[] args) {

        /*O primeiro item a entrar na stack é o primeiro a sair*/
        /*Utilizado por compiladores, como o mouse e o comando CTRL + Z */

        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Fila<Clube> fila = new Fila<>();

        System.out.println("Esta vazia? "+fila.vazia());

        fila.push(corinthians);
        fila.push(audaxOsasco);
        fila.push(botafogo);
        fila.push(oesteBarueri);
        System.out.println(fila.toString());
        System.out.println("Esta vazia? "+fila.vazia());

        fila.pop();
        System.out.println(fila.toString());
        System.out.println("Esta vazia? "+fila.vazia());


        System.out.println("------------> QUEUE");
        Queue<Clube> queue = new LinkedList<>();
        System.out.println(queue.isEmpty());
        queue.add(corinthians);
        queue.add(oesteBarueri);
        System.out.println(queue.toString());
        queue.poll();
        System.out.println(queue.toString());

    }

}
