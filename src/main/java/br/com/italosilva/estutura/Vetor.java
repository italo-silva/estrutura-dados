package br.com.italosilva.estutura;

import br.com.italosilva.domain.Clube;

import java.util.Arrays;

public class Vetor {

    private Clube[] clubes = new Clube[100];
    private int quantidadeClubes = 0;

    private void garanteEspacao(){
        if(quantidadeClubes == clubes.length){
            Clube[] novoArray = new Clube[quantidadeClubes * 2];
            for(int a = 0; a < quantidadeClubes; a++){
                novoArray[a] = clubes[a];
            }
            this.clubes = novoArray;
        }
    }

    public void adiciona(Clube clube) {
        garanteEspacao();
        clubes[quantidadeClubes]  = clube;
        quantidadeClubes++;
    }

    private boolean isPosicaoValidaSobrescrita(int posicao) {
        if(posicao >= 0 && posicao <=this.quantidadeClubes)
            return true;
        return false;
    }

    public void adiciona(int posicao, Clube clube) {
        garanteEspacao();
        if(!isPosicaoValidaSobrescrita(posicao))
            throw new IllegalArgumentException("Posicao Invalida");
        for(int a = quantidadeClubes - 1 ; a >= posicao; a--){
            clubes[a+1]  = clubes[a];
        }
        clubes[posicao] = clube;
        quantidadeClubes++;
    }


    private boolean isPosicaoValida(int posicao) {
        if(posicao >= 0 && posicao < this.quantidadeClubes)
            return true;
        return false;
    }

    public Clube pega(int posicao) {
        //recebe uma posição e devolve o clube
        if(!isPosicaoValida(posicao))
            throw new IllegalArgumentException("Posicao Invalida");
        return clubes[posicao];
    }

    public void remove(int posicao) {
        //remove pela posição
        if(!isPosicaoValida(posicao))
            throw new IllegalArgumentException("Posicao Invalida");
        for(int a = posicao; a < quantidadeClubes; a++){
            clubes[a] = clubes[a+1];
        }
    }

    public boolean contem(Clube clube) {
        for(int a = 0; a < quantidadeClubes; a ++){
            if(clubes[a].equals(clube))
                return true;
        }
        return false;
    }

    public int tamanho() {
        //devolve a quantidade de clubes
        return quantidadeClubes;
    }

    public String toString() {
        //facilitará na impressão
        return Arrays.toString(clubes);
    }
}
