package br.com.italosilva.estutura;

import java.util.LinkedList;
import java.util.List;

public class Pilha<T> {

    List<T> pilha = new LinkedList<>();

    public void push(T elemento) {
        pilha.add(elemento);
    }

    public T pop() {
        return pilha.remove(pilha.size() - 1);
    }

    public boolean vazia() {
        return pilha.isEmpty();
    }

    @Override
    public String toString() {
        return pilha.toString();
    }
}
