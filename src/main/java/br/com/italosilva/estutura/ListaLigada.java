package br.com.italosilva.estutura;

public class ListaLigada<T> {

    private Celula<T> primeira = null;
    private Celula<T> ultima = null;
    private int totalElementos = 0;

    public void adicionaNoComeco(T elemento) {

        if(this.totalElementos == 0){
            Celula nova = new Celula(elemento);
            this.primeira = nova;
            this.ultima = nova;
        } else {
            Celula nova = new Celula(elemento, this.primeira);
            this.primeira.setAnterior(nova);
            this.primeira = nova;
        }
        this.totalElementos++;

    }

    public void adiciona(T elemento) {

        if(totalElementos == 0){
            adicionaNoComeco(elemento);
        } else {
            Celula nova = new Celula(elemento);
            this.ultima.setProximo(nova);
            nova.setAnterior(this.ultima);
            this.ultima = nova;
            this.totalElementos++;
        }
    }

    private boolean isPosicaoValida(int posicao) {
        if(posicao >= 0 && posicao < totalElementos)
            return true;
        return false;
    }

    public Celula pegaCelula(int posicao) {
        if(!isPosicaoValida(posicao))
            throw new IllegalArgumentException("Posicao Invalida");

        Celula atual = primeira;
        for(int a = 0; a < posicao; a++){
            atual = atual.getProximo();
        }
        return atual;
    }

    public void adiciona(int posicao, T elemento) {

        if(posicao == 0) {
            adicionaNoComeco(elemento);
        } else if (posicao == totalElementos){
            adiciona(elemento);
        } else {
            Celula anterior = pegaCelula(posicao - 1);
            Celula nova = new Celula(elemento, anterior.getProximo());
            nova.setAnterior(anterior);
            anterior.setProximo(nova);
            this.totalElementos++;
        }
    }

    public T pega(int posicao) {

        return (T) pegaCelula(posicao).getElemento();
    }

    public void removeDoComeco() {
        if(totalElementos == 0)
            throw  new IllegalArgumentException("Tamanho inválido");

        this.primeira = primeira.getProximo();
        this.totalElementos--;

        if(this.totalElementos == 0)
            this.ultima = null;
    }

    public void removeDoFim() {
        if(totalElementos == 0)
            throw  new IllegalArgumentException("Tamanho inválido");

        if(totalElementos == 1) {
            removeDoComeco();
        } else {
            Celula penultima = this.ultima.getAnterior();
            penultima.setProximo(null);
            this.ultima = penultima;
            this.totalElementos--;
        }

    }

    public void remove(int posicao) {
        if(posicao == 0)
            removeDoComeco();
        else if(posicao == this.totalElementos -1 )
            removeDoFim();
        else {
            Celula anterior = this.pegaCelula( posicao -1 );
            Celula atual = anterior.getProximo();
            Celula proxima = atual.getProximo();
            anterior.setProximo(proxima);
            proxima.setAnterior(anterior);
            this.totalElementos--;
        }
    }

    public int tamanho() { return this.totalElementos; }

    public boolean contem(T elemento) {

        Celula atual = this.primeira;

        for(int a = 0; a < this.totalElementos; a++){
            if(atual.getElemento().equals(elemento))
                return true;
            atual = atual.getProximo();
        }
        return false;
    }

    @Override
    public String toString() {

        if(totalElementos == 0)
            return "[ ]";

        Celula atual = this.primeira;

        StringBuilder builder = new StringBuilder("[ ");
        for(int a = 0; a < totalElementos; a++){
            builder.append(atual.getElemento());
            builder.append(" , ");
            atual = atual.getProximo();
        }
        builder.append(" ]");

        return builder.toString();
    }
}
