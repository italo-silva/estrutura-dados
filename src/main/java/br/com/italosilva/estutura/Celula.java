package br.com.italosilva.estutura;

public class Celula<T> {

    private T elemento;
    private Celula<T> anterior;
    private Celula<T> proximo;

    public Celula(T elemento, Celula proxima) {
        this.elemento = elemento;
        this.proximo = proxima;
    }

    public Celula(T elemento) {
        this(elemento, null);
    }

    public void setAnterior(Celula<T> anterior) {
        this.anterior = anterior;
    }

    public Celula<T> getAnterior() {
        return anterior;
    }

    public void setProximo(Celula proximo) {
        this.proximo = proximo;
    }

    public Celula getProximo() {
        return this.proximo;
    }

    public T getElemento() {
        return elemento;
    }

}
