package br.com.italosilva.estutura;

import java.util.LinkedList;
import java.util.List;

public class Fila<T> {

    List<T> fila = new LinkedList<>();

    public void push(T elemento) {
        fila.add(elemento);
    }

    public T pop() {
        return fila.remove(0);
    }

    public boolean vazia() {
        return fila.isEmpty();
    }

    @Override
    public String toString() {
        return fila.toString();
    }
}
