package br.com.italosilva.domain;

import java.util.Objects;

public class Clube {

    private String nome;

    private int pontos;

    public Clube(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
    }

    public String getNome() {
        return nome;
    }

    public int getPontos() {
        return pontos;
    }

    @Override
    public String toString() {
        return new StringBuilder("Clube : ")
                .append(nome)
                .append(" Pontos: ")
                .append(pontos)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clube clube = (Clube) o;
        return pontos == clube.pontos &&
                Objects.equals(nome, clube.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, pontos);
    }
}
