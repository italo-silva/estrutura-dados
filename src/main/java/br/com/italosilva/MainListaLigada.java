package br.com.italosilva;

import br.com.italosilva.domain.Clube;
import br.com.italosilva.estutura.ListaLigada;

public class MainListaLigada {

    public static void main(String[] args) {

        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);
        Clube desportivoBrasil = new Clube("Desportivo Brasil", 7);
        Clube saoCaetano = new Clube("São Caetano", 7);

        ListaLigada<Clube> listaLigada = new ListaLigada<>();

        listaLigada.adicionaNoComeco(corinthians);
        System.out.println(listaLigada);

        listaLigada.adicionaNoComeco(botafogo);
        System.out.println(listaLigada);

        listaLigada.adiciona(oesteBarueri);
        System.out.println(listaLigada);

        listaLigada.adiciona(1 , audaxOsasco);
        System.out.println(listaLigada);

        Clube x = listaLigada.pega(0);
        System.out.println("------> Pega : "+x);

        int totalElementos = listaLigada.tamanho();
        System.out.println("------> Tamanho : "+totalElementos);

        listaLigada.removeDoComeco();
        System.out.println(listaLigada);

        totalElementos = listaLigada.tamanho();
        System.out.println("------> Tamanho : "+totalElementos);

        listaLigada.removeDoFim();
        System.out.println(listaLigada);

        totalElementos = listaLigada.tamanho();
        System.out.println("------> Tamanho : "+totalElementos);

        listaLigada.adicionaNoComeco(desportivoBrasil);
        System.out.println(listaLigada);

        listaLigada.adicionaNoComeco(saoCaetano);
        System.out.println(listaLigada);

        listaLigada.remove(2);
        System.out.println(listaLigada);

        totalElementos = listaLigada.tamanho();
        System.out.println("------> Tamanho : "+totalElementos);

        boolean contemElemento = listaLigada.contem(corinthians);
        System.out.println("------> Contem elemento: "+contemElemento);

    }
}
