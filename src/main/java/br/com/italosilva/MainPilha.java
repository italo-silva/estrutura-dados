package br.com.italosilva;

import br.com.italosilva.domain.Clube;
import br.com.italosilva.estutura.Pilha;

import java.util.Stack;

public class MainPilha {

    public static void main(String[] args) {

        /*O último item a entrar na stack é o primeiro a sair*/
        /*Utilizado por compiladores, como o mouse e o comando CTRL + Z */

        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Pilha<Clube> pilha = new Pilha<>();

        System.out.println("Esta vazia? "+pilha.vazia());

        pilha.push(corinthians);
        pilha.push(audaxOsasco);
        pilha.push(botafogo);
        pilha.push(oesteBarueri);
        System.out.println(pilha.toString());
        System.out.println("Esta vazia? "+pilha.vazia());

        pilha.pop();
        System.out.println(pilha.toString());
        System.out.println("Esta vazia? "+pilha.vazia());

        System.out.println("------------> STACK");
        Stack<Clube> stack = new Stack<>();
        System.out.println(stack.isEmpty());
        stack.push(corinthians);
        stack.push(oesteBarueri);
        System.out.println(stack.toString());
        stack.pop();
        System.out.println(stack.toString());

    }

}
